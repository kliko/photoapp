import {NgModule} from "@angular/core";
import {ImagesService} from "./services/images.service";

@NgModule({
    providers: [
        ImagesService
    ]
})
export class AppModule { }
