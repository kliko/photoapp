import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Config} from "../../config/config";

@Injectable()
export class ImagesService {
    private resourceUrl: string = '/images?format=json&nojsoncallback=?';

    constructor(private http: HttpClient) {
    }

    /**
     * Gets list with all images
     * @returns {Observable<Object>}
     */
    public index() {
        return this.http.get(`${Config.API_ENDPOINT}${this.resourceUrl}`)
    }

    /**
     *
     * @param searchQuery
     * @returns {Observable<Object>}
     */
    public search(searchQuery: any) {
        return this.http.get(`${Config.API_ENDPOINT}${this.resourceUrl}&q${searchQuery}`);
    }
}