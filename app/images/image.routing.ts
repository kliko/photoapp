import {NgModule} from "@angular/core";
import {NativeScriptRouterModule} from "nativescript-angular/router";
import {Routes} from "@angular/router";

import {ImageListComponent} from "./components/image-list.component";

const routes: Routes = [
    {path: "images", component: ImageListComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: []
})
export class AppRoutingModule {
}