import {Component, OnInit} from "@angular/core";
import {ImagesService} from "../../shared/services/images.service";
import {Observable} from "rxjs/Observable";
@Component({
    selector: 'image-list',
    templateUrl: 'image-list.component.html'
})
export class ImageListComponent implements OnInit {
    public images$: Observable<any>;

    constructor(public imageService: ImagesService) {
    }

    public ngOnInit() {
        this.images$ = this.imageService.index();
    }

    public search(query) {
        this.images$ = this.imageService.search(query);
    }
}