import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {ImageListComponent} from "./components/image-list.component";

@NgModule({
    imports: [],
    exports: [
        ImageListComponent
    ],
    declarations: [
        ImageListComponent
    ],
    providers: [],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ImageModule {
}
